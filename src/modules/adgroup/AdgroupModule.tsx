import React = require("react");
import ReactDOM = require("react-dom");
import BaseModule = require("core/module/AbstractModule");
import Header = require("components/common/header");
import Navigation = require("components/common/navigation");
import Footer = require("components/common/footer");
import KeywordsTableModule = require("components/common/keywords/keyword-table");
import _ = require("underscore");

import BaseLayoutModule = require("core/module/BaseLayoutModule");
var DUMMY_LAYOUT = (function () {
  var div: HTMLElement = document.createElement("div");

  div.innerHTML = `
    <div class="container" data-hook-header-region></div>
    <div class="container">
      <div class="row">
        <div class="left-wrapper" data-hook-navigation-region></div>
        <div class="content-wrapper" data-hook-content-region>
          <div class="panel-body" data-hook-adgroup-list></div>
          <div class="panel-body" data-hook-adgroup-keywords-list></div>
          <div class="panel-body" data-hook-adgroup-minus-words></div>
        </div>
      </div>
    </div>
    <div class="container" data-hook-footer-region></div>
  `;

  return div;
})();

class NSTDLayoutController extends BaseLayoutModule.BaseLayoutController {
  constructor() {
    super({
      rootNode: document.body,
      layoutNode: DUMMY_LAYOUT,
      containerNodesSpecification: {
        "header": "[data-hook-header-region]",
        "navigation": "[data-hook-navigation-region]",
        "content": "[data-hook-content-region]",
        "footer": "[data-hook-footer-region]",
        "keywords-table": "[data-hook-adgroup-keywords-list]"
      }
    });

    this.renderNavigation();
    this.renderHeader();
    this.renderFooter();
    this.renderContent();
  }

  renderNavigation(): void {
    var NavigationComponent = Navigation.Navigation;
    ReactDOM.render(<NavigationComponent />, this.getRegion("navigation"));
  }

  renderHeader(): void {
    var HeaderComponent = Header.Header;
    ReactDOM.render(<HeaderComponent />, this.getRegion("header"));
  }

  renderContent(): void {
		var KeywordsTableComponent = KeywordsTableModule.KeywordsTable;
    ReactDOM.render(<KeywordsTableComponent />, this.getRegion("keywords-table"));
  }

  renderFooter(): void {
    var FooterComponent = Footer.Footer;
    ReactDOM.render(<FooterComponent />, this.getRegion("footer"));
  } }

class AdgroupModule extends BaseModule {

  layoutController: BaseLayoutModule.BaseLayoutController;

  constructor() {
    super();
  }

  initialize(): any {
    this.layoutController = new NSTDLayoutController();
  }

  destroy(): any {
    console.log("destroy module!");
  }

}

export = AdgroupModule;
