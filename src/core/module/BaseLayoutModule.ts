import React = require("react");
import ReactDOM = require("react-dom");
import jQuery = require("jquery");
import _ = require("underscore");

/*
* Контролирует рендеринг компонентов модуля
* Содержит в себе "регионы" - HTMLElement, в который будет встраиваться react-компоненты
*/

export interface ContainerNodesSpecification {
  [key: string]: any;
}

export interface LayoutControllerOptions {
  containerNodesSpecification?: ContainerNodesSpecification;
  rootNode: Element;
  layoutNode: Element;
}

export class BaseLayoutController {

  private DOMRegions: {};
  private rootNode: Element;
  private layoutNode: Element;

  constructor(options: LayoutControllerOptions) {
    this.DOMRegions = {};

    this.attachToDOM({
      root: options.rootNode,
      layout: options.layoutNode
    });

    this.initializeRegions(options.containerNodesSpecification);
  }

  attachToDOM(options: {root: Element, layout: Element}): void {
    this.rootNode = options.root;
    this.layoutNode = options.layout;
    this.rootNode.appendChild(this.layoutNode);
  }

  addContainerNode(containerName: string, DOMNode: Element): void {
    if (typeof this.DOMRegions[containerName] !== "undefined") {
      throw new Error("DOM Container node with name " + containerName + " already exists!");
    }

    this.DOMRegions[containerName] = DOMNode;
  }

  initializeRegions(containerNodesSpecifications: ContainerNodesSpecification): void {
    _.each<any>(containerNodesSpecifications, function (selector: string, name: string) {
      var node:Element = this.getDOMNode(this.layoutNode, selector);
      this.addContainerNode(name, node);
    }, this);
  }

  getDOMNode(parent, childSelector): jQuery {
    return parent.querySelector(childSelector);
  }

  destroy(): void {
    _.each<any>(this.DOMRegions, function (node: Element, key) {
      ReactDOM.unmountComponentAtNode(node);
    });
    jQuery(this.layoutNode).remove();
    this.DOMRegions = {};
  }

  checkRegion(regionName: string): boolean {
    return typeof this.DOMRegions[regionName] !== "undefined";
  }

  getRegion(name: string): HTMLElement {
    if (this.checkRegion(name) === false) {
      throw new Error("Trying to access nonexistent region");
    }
    return this.DOMRegions[name];
  }

}
