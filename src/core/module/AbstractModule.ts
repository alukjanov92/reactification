import BaseLayoutModule = require("core/module/BaseLayoutModule");

interface BaseModuleOptions {
  layoutController?: BaseLayoutModule.BaseLayoutController;
}

abstract class BaseModule {
  layoutController: BaseLayoutModule.BaseLayoutController;

  abstract initialize(): any; //@TODO Deferred
  abstract destroy(): any; //@TODO Deferred
}

export = BaseModule;
