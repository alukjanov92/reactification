import AbstractModule = require("core/module/AbstractModule");
import AdgroupModule = require("modules/adgroup/AdgroupModule");

interface ModuleLoaderLifecycle {
  loadModule(module: string): void;
  initializeModule(module: AbstractModule): any;
  disposeCurrentModule(): void;
}

class ModuleLoader implements ModuleLoaderLifecycle {
  currentModule: AbstractModule; //null on initialization

  constructor() {
    this.currentModule = null;
  }

  loadModule(name: string): void {
    if (this.currentModule) {
      this.disposeCurrentModule();
    }

    this._loadModule(name);
  }

	private _loadModule(name): any {
		var module = new AdgroupModule();
		this.initializeModule(module);
	}

  initializeModule(module: AbstractModule): any { //@TODO Deferred possible
		module.initialize();
  }

	initializeModuleLayout(module: AbstractModule): void {
		var layoutController = this.getLayoutController(module);
	}

	getLayoutController(module: AbstractModule): any {
		return null;
	}

  disposeCurrentModule(): void {
    if (typeof this.currentModule === "undefined") {
      throw new Error("Cannot dispose current module, current module is undefined");
    }

    this.currentModule.destroy();
  }

}

export = ModuleLoader;
