import ModuleLoader = require("core/module/ModuleLoader");
import jQuery = require("jquery");

class Application {
  currentModule: any;
  moduleLoader: any;
  router: any;

  initialize(): void {
    this.initializeModuleLoader();
    this.initializeRouter();
  }

  initializeModuleLoader(): void{
    this.moduleLoader = new ModuleLoader();
  }

  initializeRouter(): void {
    this.router = null; //@TODO
  }

}


jQuery(document).ready(function () {
	var app:Application = new Application();
	app.initialize();
	app.moduleLoader.loadModule("adgroup");
});
