import React = require("react");
import ReactDOM = require("react-dom");

export class Header extends React.Component<any, any> {
  render(): JSX.Element {
    return (
      <header>
         <a href="/" id="logo"></a>
         <div className="inner">
            <div className="header_contacts">
               <span>
                <i className="phone_ico"></i>
                <span className="main-phone-number">8 800 333 58 78</span>
                <span className="arr"></span>
               </span>
              <a href="mailto:help@aori.ru" className="">
                <span className="ng-scope">help@aori.ru</span>
              </a>
            </div>
            <div className="info-account ng-binding">
               <span className="ng-scope">На счету</span>
               : <i className="money-ico"></i> <a href="/app/#/balance/payment" className="fs20 c_3290d1 tdn ng-binding">49&nbsp;746,00</a>&nbsp;₽<br />
               <div className="fr">
                 <a className="tdn c_3290d1 hover_tdu fs12" href="/app/#/balance/payment">
                   <span className="ng-scope">Пополнить счет</span>
                 </a>
               </div>
            </div>
            <div className="userinfo fs13 t-userinfo-block">
               <div className="details">
                  <div className="button t-userinfo-button">
                    <span className="email ng-binding">user@example.com</span>
                    <span className="arr"></span>
                  </div>
                  <ul className="t-userinfo-expander-block">
                    <li>
                        <a href="/app/#/settings/profile" className="t-profile-link"><span className="ng-scope">Аккаунт</span></a>
                    </li>
                    <li>
                      <a href="/profile/logout" className="cp t-logout-link" >
                        <span>Выход</span>
                      </a>
                    </li>
                  </ul>
               </div>
            </div>
         </div>
      </header>
    );
  }
}
