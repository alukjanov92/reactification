import React = require("react");
import ReactDOM = require("react-dom");

export class Footer extends React.Component<any, any> {
  render(): JSX.Element {
    return (
      <footer className="w1246 common">
        <nav className="ng-scope">
          <ul>
            <li><a href="/about_aori/"><span className="ng-scope">О системе</span></a></li>
            <li><a href="/about_contextual/"><span className="ng-scope">О контекстной рекламе</span></a></li>
            <li><a href="/learning_center/"><span className="ng-scope">Учебный центр</span></a></li>
          </ul>
          <ul>
            <li><a href="/about_aori/company/"><span>О компании</span></a></li>
            <li><a href="/news/"><span>Новости</span></a></li>
          </ul>
          <ul>
            <li><a href="/feedback/"><span>Обратная связь</span></a></li>
            <li><a href="/company/contacts/"><span>Работа у нас</span></a></li>
          </ul>
          <ul>
            <li><a href="/company/contacts/"><span>Контакты</span></a></li>
            <li><a href="/partners/"><span>Партнерам</span></a></li>
          </ul>
        </nav>
        <div className="contacts">
          <span className="tel"><span>8 800 333-58-78</span></span>
          <div><span>служба поддержки</span></div>
          <ul className="social">
            <li><a href="http://www.facebook.com/Aori.ru" className="fb"></a></li>
            <li><a href="http://vk.com/aori_ru" className="vk"></a></li>
            <li><a href="http://twitter.com/aori_ru" className="tw"></a></li>
            <li><a href="http://www.youtube.com/user/AoriRu" className="yt"></a></li>
            <li><a href="https://plus.google.com/109761993025121319039" rel="publisher" className="googlepl_ico" title="Google plus"></a></li>
          </ul>
        </div>
      </footer>
    );
  }
}
