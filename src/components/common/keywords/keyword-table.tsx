import React = require("react");
import ReactDOM = require("react-dom");
import _ = require("underscore");
import BlockTableRow = require("components/common/keywords/row");
import DummyKeywords = require("data/dummy/keywords-collection");

var kw = (function () {
	var _kw =  _.pick(DummyKeywords, Object.keys(DummyKeywords).slice(0, 100)),
			ret = [];

	_.each(_kw, function (kw) {
		ret.push(kw);
	});

	return ret;
})();


export class KeywordsTable extends React.Component<any, any> {
	state: {keywords: Array<any>} = {
		keywords: kw
	};

	currentRowId: number = 0;

  constructor(props, state) {
    super(props, state);
    this.currentRowId = 0;
  }

	sortKeywords(): void {
		var keywords = this.state.keywords;
		this.setState({
			keywords: keywords.reverse()
		});
	}


  render(): JSX.Element {
    return (
      <div className="base-block-table keywords-list-table">
        {this.renderTableHeader()}
        <div className="body">{this.renderTableBody()}</div>
        {this.renderTableFooter()}
      </div>
    );
  }

  renderTableHeader(): JSX.Element {
    return (
      <div className="header">
		    <div className="table-row">
		    	<div className="col colspan3-col"></div>
		    	<div className="col yandex-col">
		    		<span className="yandex-icon"></span>
		    		<span>Яндекс</span>
		    	</div>
		    	<div className="col google-col">
		    		<span className="google-icon"></span>
		    		<span>Яндекс</span>
		    	</div>
		    </div>
		    <div className="table-row">
		    	<div className="col checkbox-col"></div>
		    	<div className="col status-col">Статус</div>
		    	<div className="col keyword-col">Ключевое слово</div>
		    	<div className="col yandex-forecast-col sortable">Ст-ть позиций, руб.</div>
		    	<div className="col yandex-bid-col sortable">Ставка, руб.</div>
		    	<div className="col google-forecast-col sortable">Ст-ть позиций, руб.</div>
		    	<div className="col google-bid-col sortable">Ставка, руб.</div>
		    </div>
	    </div>
    );
  }

  renderTableBody(): JSX.Element[] {
    return _.map(this.state.keywords, function (keyword) {
      var id = ++this.currentRowId;

			var cellTypes = [
				"checkbox", "status", "keyword", "forecast_direct", "bid_direct", "forecast_adwords", "bid_adwords", "controls"
			];
      return <BlockTableRow cellTypes={cellTypes} keyword={keyword} key={id} />
    }, this);
  }

  renderTableFooter(): JSX.Element {
    return (<div className="footer"></div>);
  }

}
