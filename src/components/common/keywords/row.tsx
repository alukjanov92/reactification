import React = require("react");
import ReactDOM = require("react-dom");
import _ = require("underscore");
var formatters = { //@todo export
	formatNumber: function (number, precision) {
		return number.toFixed(precision);
	}
};

interface KeywordCellProp {
	keyword: any;
	key: number;
}

class CheckboxCell extends React.Component<KeywordCellProp, any> {
  render(): JSX.Element {
    return (
      <div className="col checkbox-col">
        <input type="checkbox" className="item-checkbox" />
        <i className="aori-icon aori-icon-attention"></i>
      </div>
    );
  }
}

class StatusCell extends React.Component<KeywordCellProp, any> {
  render(): JSX.Element {
    return (
      <div className="col status-col">
        <div className="status2">
          <span className="yandex">
            <i className="moderation-icon waiting"></i>
          </span>

          <span className="google">
            <i className="moderation-icon waiting"></i>
          </span>
        </div>
      </div>
    );
  }
}

class KeywordCell extends React.Component<KeywordCellProp, any> {
  render(): JSX.Element {
    var text = this.props.keyword.text;
    var groupName = this.props.keyword.groupName;

    return (
      <div className="col keyword-col">
        <div className="keyword-holder">
          <div className="keyword-text">
            <span>{text}</span>
            <i></i>
          </div>
					{groupName ? <div className="adgroup-name-container">{groupName}</div> : null}

        </div>
      </div>
    );
  }
}

interface ForecastCellProps {
	engine: string;
	key: number;
	keyword: any;
}

class ForecastCell extends React.Component<KeywordCellProp, any> {
	state: {hasForecast: boolean};
	constructor(props, state) {
		super(props, state);
		var engine = this.props.engine;
		this.state = {
			hasForecast: this.hasForecast()
		};
	}

	hasForecast() {
		var fc = this.getForecast();
		return _.size(fc) !== 0;
	}

	getForecast() {
		var engine = this.props.engine;
		var rawForecast = this.props.keyword.forecast_bids[engine === "direct" ? 0 : 1];
		return _.pick(rawForecast, "max", "min", "max_first", "min_first");
	}

  render(): JSX.Element {;
		var engine = this.props.engine;
		var className = engine === "direct" ? "yandex-forecast-col" : "google-forecast-col";

      return (
        <div className={"col " + className}>
          <div className="forecast-holder">
            {this.renderForecastList()}
          </div>
        </div>
      );
  }

  renderForecastList() {
    if (this.state.hasForecast === true) {
      var forecast = this.getForecast();
      return (
        <dl className="dl-horizontal">
          {
            _.map(forecast, function (value, key) {
              return [
                <dt>{key}</dt>,
                <dd>{value}</dd>
              ];
            })
          }
        </dl>
      );
    } else {
      return null;
    }
  }
}

interface BidCellProps {
	keyword: any;
	engine: string;
	key: number;
}

class BidCell extends React.Component<BidCellProps, any> {
  render(): JSX.Element {
		var engine = this.props.engine;
		var idx = engine == "adwords" ? 1 : 0;
		var rawBid = this.props.keyword.bids[idx];
		if (rawBid === undefined) {
			return null;
		}
		var bid = formatters.formatNumber(rawBid.bid, 2);
		var className = engine === "adwords" ? "google-bid-col" : "yandex-bid-col";

    return (
      <div className={"col " + className}>
        <div>
          <div className="clickable-holder">
            <span>{bid}</span>
            <i className="aori-icon aori-icon-lock2"></i>
            {this.renderStrategy()}
          </div>
        </div>
      </div>
    );
  }

	renderStrategy() {
			return null;
	}

}

class ControlsCell extends React.Component<KeywordCellProp, any> {
  render(): JSX.Element {
    return (
      <div className="col controls-col">
        <div className="instant-controls stop delete">
          <button className="btn btn-default">
            <i className="aori-icon aori-icon-play2"></i>
            <i className="aori-icon aori-icon-pause2"></i>
          </button>
        </div>
      </div>
    );
  }
}

interface BlockTableRowProps {
	keyword: any;
	cellTypes: string[]
	key: number;
}

class BlockTableRow extends React.Component<BlockTableRowProps, any> {
	state: {cells: Array<string>} = {
		cells: this.mapCells()
	}

  mapCells(): Array<any> {
		return JSON.parse(JSON.stringify(this.props.cellTypes));
  }

  render(): JSX.Element {
    return (
      <div className="table-row">
        <div className="cols-holder">
          {this.renderCells()}
        </div>
      </div>
    );
  }

  renderCells(): JSX.Element[] {
    return _.map(this.state.cells, function (type, idx) {
      return this.renderCell(type, idx);
    }, this);
  }

  renderCell(type, idx): JSX.Element {
    var CellComponent: new() => React.Component<KeywordCellProp, any>;
		var keyword = this.props.keyword;

    switch (type) {
      case "bid_adwords":		return <BidCell keyword={keyword} engine="adwords" key={idx} />;

      case "bid_direct":		return <BidCell keyword={keyword} engine="direct" key={idx} />;

      case "forecast_direct":			return <ForecastCell keyword={keyword} key={idx} engine="direct" />;

      case "forecast_adwords":			return <ForecastCell keyword={keyword} key={idx} engine="adwords" />;

      case "status":    		return <StatusCell keyword={keyword} key={idx} />;

      case "checkbox":  		return <CheckboxCell keyword={keyword} key={idx} />;

      case "control":   		return <ControlsCell keyword={keyword} key={idx} />;

      case "keyword":   		return <KeywordCell keyword={keyword} key={idx} />;

      default:							return null;
    }

  }

}

export = BlockTableRow;
