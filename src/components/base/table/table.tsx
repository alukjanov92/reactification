import ReactDOM = require("react-dom");
import React = require("react");
import CellsModule = require("components/base/table/cells");
import RowsModule = require("components/base/table/rows");
import _ = require("underscore");

export interface TableProps {
  columns: Array<CellsModule.CellSpecification>;
  rows: Array<RowsModule.TableRowCellsSpecification>;
}

export class Table extends React.Component <TableProps, {}> {
  render(): JSX.Element {
    var columns = this.props.columns;
    var rows = this.props.rows;
    var columnElements: Array<React.ReactChild> = _.map(columns, function (cellSpecification) {
          var name = cellSpecification.cellData.label,
              id = cellSpecification.cellData.id;
          return (<tr key={Math.random() * 1000}>{name}</tr>);
    });
    var rowElements: Array<React.ReactChild> = _.map(rows, function (row) {
      return (
        <RowsModule.TableRow cellsData={row} key={Math.random() * 100000}/>
      );
    });
    return (
      <table>
        <thead>
          {columnElements}
        </thead>
        <tbody>
          {rowElements}
        </tbody>
      </table>
    );
  }
}
