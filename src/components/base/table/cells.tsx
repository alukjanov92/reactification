import ReactDOM = require("react-dom");
import React = require("react");

export interface CellOptions {
  cellType?: String;
  cellComponent?: new () => React.Component<any, any>;
}

export interface CellSpecification {
  cellOptions: CellOptions;
  cellData: any; //@TODO data typings
}

export class TableCell extends React.Component<any, any> {
  render(): JSX.Element {
    var label = this.props.data.label;

    return (
      <td>{label}</td>
    );
  }
}

export class TableCellEditPopup extends React.Component<any, any> {
  render(): JSX.Element {
    return (
      <div className="tableCellEditBlock">
        <div className="inputBlock">
          <input className="form-control" ref="input" />
        </div>
        <div className="controls-block">
          <button className="btn btn-primary" onClick={this.onSave}>Сохранить</button>
          <button className="btn btn-default" onClick={this.onCancel}>Отмена</button>
        </div>
      </div>
    );
  }

  onSave(): void {
    console.log(this.getValue() + " saved!");
  }

  onCancel(): void {
    console.log(this.getValue() + " cancelled!");
  }

  getValue(): string {
		var node = ReactDOM.findDOMNode<HTMLInputElement>(this.refs["input"]);
    return node.value;
  }
}

interface EditableLabelCellState {
  editBlockShown: boolean;
}

export class EditableLabelCell extends React.Component<CellSpecification, EditableLabelCellState> {

  constructor(props: CellSpecification) {
		super(props);

    this.state = {
      editBlockShown: false
    };
  }

  render(): JSX.Element {
    var label = this.props.cellData.label;

    return (
      <td>
        <div className="clickable-holder" onClick={this.handleClick}>{label}</div>
        {this.state.editBlockShown ? <TableCellEditPopup /> : null}
      </td>
    );
  }

  handleClick(e): void {
    var isCurrentlyShown = this.state.editBlockShown;
    this.setState({editBlockShown: !isCurrentlyShown}); //@TODO Global click listeners
  }

}


