import ReactDOM = require("react-dom");
import React = require("react");
import CellsModule = require("components/base/table/cells");
import _ = require("underscore");

export type TableRowCellsSpecification = Array<CellsModule.CellSpecification>;

interface TableRowProps {
	cellsData: TableRowCellsSpecification;
  key: number;
};

export class TableRow extends React.Component<TableRowProps, any> {
  render(): JSX.Element {
    var data = this.props.cellsData,
        cells = this.renderCells(data);

    return (
      <tr>{this.renderCells(data)}</tr>
    );
  }

  renderCells(cellsData: Array<CellsModule.CellSpecification>): Array<JSX.Element> {
    var cells = _.map(cellsData, function (cellSpecification: CellsModule.CellSpecification) {
      var CellComponent: new () => React.Component<any, any>,
          cellOptions: CellsModule.CellOptions = cellSpecification.cellOptions,
          cellData: {} = cellSpecification.cellData,
          cellType: String = cellOptions.cellType;

      if (typeof cellType === "undefined") {
        CellComponent = cellOptions.cellComponent;
      } else {
        CellComponent = this.getPredefinedCellComponent(cellType);
      }

      return (
        <CellComponent data={cellData} options={cellOptions} key={Date.now() * Math.random()} />
      );
    }, this);

    return cells;
  }

  getPredefinedCellComponent(cellType): new () => React.Component<any, any> {
    return CellsModule.TableCell;
  }

}
