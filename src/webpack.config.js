path = require("path");
var jsSourceFolder = path.resolve(__dirname, "../build/"),
		nstdApplicationEntryPoint = path.resolve(jsSourceFolder, "app.js"),
		webModulesPath = path.resolve(jsSourceFolder, "modules/"),
		bundledResultFolder = path.resolve(__dirname, "../bundled/");

module.exports = {
	entry: nstdApplicationEntryPoint,

	output: {
		filename: "bundle.js",
		path: bundledResultFolder
	},

	resolve: {
		modulesDirectories: [__dirname + "/node_modules/", jsSourceFolder, "node_modules"]
	}
};
